# Projeto criado para desafio técnico do Banco Inter.

**Tecnologias utilizadas para construção do projeto Java versão 11, utilizando o framework Spring Boot versão LTS.
Foi Construída uma API REST para o desafio proposto. O mesmo possui uma documentação swagger porem só estará habilitada quando o projeto estiver devidamente configurado e rodando localmente em sua maquina (URL:  localhost:8080/swagger-ui.html#/  )**

## Configurando projeto

inicialmente temos que rodar o maven para que o mesmo baixe todas as dependências sendo essas JPA,SpringFox, Swagger, H2, JUnit para construção dos testes unitários. Após isso você pode inicializar o projeto que deve esta rodando na porta localhost:8080

## Principais recursos da aplicação
-  Usuario: localhost:8080/usuarios apenas possui método post
-  Empresa: localhost:8080/empresas possui os métodos post,get,put,delete sendo que os dois últimos temos que passar o id pela URL
-  Comprar Ações: localhost:8080/comprarAcoes possui métodos post e o get 


## Testes unitários
 foram feitos das classes e métodos que julgo importante para o funcionamento do software.

## Configurando arquivo application.properties para utilização do banco

- spring.h2.console.enabled=true
- spring.datasource.url=jdbc:h2:mem:crm
- spring.datasource.driverClassName=org.h2.Driver
- spring.datasource.username=sa
- spring.datasource.password=
- spring.jpa.show-sql=true
- spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.H2Dialect
- spring.jpa.hibernate.ddl-auto=update

