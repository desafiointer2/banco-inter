package com.banco.inter.converter;

import com.banco.inter.dto.UsuarioDTO;
import com.banco.inter.model.Usuario;
import com.banco.inter.repository.UsuarioRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
public class ConverterUsuarioImplTest {


    private ConverterUsuarioImpl converterUsuario = new ConverterUsuarioImpl();

    @MockBean
    private UsuarioRepository  usuarioRepository;

    @Test
    public void deveConverterUmUsuarioParaUsuarioDto(){

        Mockito.when(usuarioRepository.converterUsuarioEntidadeParaUsuarioDTO(getUsuario())).thenReturn(getUsuarioDTO());

        UsuarioDTO usuarioDTOTest = this.converterUsuario.converterUsuarioEntidadeParaUsuarioDTO(getUsuario());


        assertTrue(usuarioDTOTest.getId().equals(1l));
        assertTrue(usuarioDTOTest.getNome().equals("caio"));
        assertTrue(usuarioDTOTest.getCpf().equals("1"));
        assertTrue(usuarioDTOTest.getCarteira().equals(new BigDecimal(100)));

    }
    @Test
    public void deveConverterUmUsuarioDTOParaUsuario(){

        Mockito.when(usuarioRepository.converterUsuarioDTOParaUsuarioEntidade(getUsuarioDTO())).thenReturn(getUsuario());
        Usuario usuario = this.converterUsuario.converterUsuarioDTOParaUsuarioEntidade(getUsuarioDTO());

        assertTrue(usuario.getId().equals(1l));
        assertTrue(usuario.getNome().equals("caio"));
        assertTrue(usuario.getCpf().equals("1"));
        assertTrue(usuario.getCarteira().equals(new BigDecimal(100)));

    }
    private Usuario getUsuario(){
        Usuario usuario = new Usuario();
        usuario.setId(1l);
        usuario.setNome("caio");
        usuario.setCpf("1");
        usuario.setCarteira(new BigDecimal(100));
        return usuario;
    }

    private UsuarioDTO getUsuarioDTO(){
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setId(1l);
        usuarioDTO.setNome("caio");
        usuarioDTO.setCpf("1");
        usuarioDTO.setCarteira(new BigDecimal(100));
        return usuarioDTO;
    }
}