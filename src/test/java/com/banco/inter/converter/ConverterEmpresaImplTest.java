package com.banco.inter.converter;

import com.banco.inter.dto.EmpresaDTO;
import com.banco.inter.model.Empresa;
import com.banco.inter.repository.EmpresaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static com.banco.inter.model.enuns.StatusEmpresa.ATIVO;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
public class ConverterEmpresaImplTest {

    ConverterEmpresaImpl converterEmpresa = new ConverterEmpresaImpl();

    @MockBean
    private EmpresaRepository empresaRepository;

    @Test
    public void deveConverterUmEmpresaParaEMpresaDto(){

        Mockito.when(empresaRepository.converterEmpresaDTOParaEmpresaEntidade(getEmpresaDTO())).thenReturn(getEmpresa());
        EmpresaDTO empresaDTOtest =  this.converterEmpresa.converterEmpresaEntidadeParaEmpresaDTO(getEmpresa());

        assertTrue(empresaDTOtest.getId().equals(1l));
        assertTrue(empresaDTOtest.getAcao().equals("b1"));
        assertTrue(empresaDTOtest.getTicket().equals("apple"));
        assertTrue(empresaDTOtest.getPreco().equals(new BigDecimal(100)));
        assertTrue(empresaDTOtest.getStatus().equals(ATIVO));
        assertTrue(empresaDTOtest.getPercentualAcoes() == 100);

    }
    @Test
    public void deveConverterUmEmpresaDTOparaEmpresa(){

        Mockito.when(empresaRepository.converterEmpresaEntidadeParaEmpresaDTO(getEmpresa())).thenReturn(getEmpresaDTO());
        Empresa empresa = this.converterEmpresa.converterEmpresaDTOParaEmpresaEntidade(getEmpresaDTO());

        assertTrue(empresa.getId().equals(1l));
        assertTrue(empresa.getAcao().equals("b1"));
        assertTrue(empresa.getTicket().equals("apple"));
        assertTrue(empresa.getPreco().equals(new BigDecimal(100)));
        assertTrue(empresa.getStatus().equals(ATIVO));
        assertTrue(empresa.getPercentualAcoes() == 100);


    }

    private Empresa getEmpresa(){
        Empresa empresa = new Empresa();
        empresa.setId(1l);
        empresa.setAcao("b1");
        empresa.setTicket("apple");
        empresa.setPreco(new BigDecimal(100));
        empresa.setStatus(ATIVO);
        empresa.setPercentualAcoes(100);
        return empresa;
    }
    private EmpresaDTO getEmpresaDTO() {
        EmpresaDTO empresaDTO = new EmpresaDTO();
        empresaDTO.setId(1l);
        empresaDTO.setAcao("b1");
        empresaDTO.setTicket("apple");
        empresaDTO.setPreco(new BigDecimal(100));
        empresaDTO.setStatus(ATIVO);
        empresaDTO.setPercentualAcoes(100);
        return empresaDTO;
    }
}