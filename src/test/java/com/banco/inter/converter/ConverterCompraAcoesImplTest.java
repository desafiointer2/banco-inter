package com.banco.inter.converter;

import com.banco.inter.dto.ComprarAcoesDTO;
import com.banco.inter.dto.InputComprarAcoesDTO;
import com.banco.inter.dto.TotalRendimentosEmpresaDTO;
import com.banco.inter.model.ComprarAcoes;
import com.banco.inter.model.Empresa;
import com.banco.inter.model.Usuario;
import com.banco.inter.repository.ComprarAcoesRepository;
import com.banco.inter.repository.EmpresaRepository;
import com.banco.inter.repository.UsuarioRepository;
import com.banco.inter.service.ComprarAcoesService;
import com.banco.inter.service.EmpresaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static com.banco.inter.model.enuns.StatusEmpresa.ATIVO;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConverterCompraAcoesImplTest {


    @Autowired
    public ComprarAcoesRepository comprarAcoesRepository;
    @Autowired
    public EmpresaRepository empresaRepository;

    @Autowired
    public UsuarioRepository usuarioRepository;

    public ComprarAcoesService comprarAcoesService ;

    @Before
    public void setUp(){
        comprarAcoesService = new ComprarAcoesService(usuarioRepository, empresaRepository,comprarAcoesRepository);
        this.usuarioRepository.save(getUsuario());
        this.empresaRepository.save(getEmpresa());
        this.comprarAcoesRepository.save(getComprarAcoes());
    }

    @Test
    public void deveConverterUmComprarAcoesParaComprarAcoesDto(){
        InputComprarAcoesDTO inputComprarAcoesDTO = new InputComprarAcoesDTO(1l,1l);

        ComprarAcoesDTO dto = this.comprarAcoesService.comprarAcoes(inputComprarAcoesDTO);

        assertTrue(dto.getId() != null);

    }
    @Test
    public void deveListarAcoesCompradas(){
        List<ComprarAcoesDTO> dtos = this.comprarAcoesService.listaDeAcoesCompradas(1l);

        assertFalse(dtos.size() == 0);

    }
    @Test
    public void deveTestarPorAcoesInvestidas(){
        List<ComprarAcoesDTO> dtos = this.comprarAcoesService.listaPorEmpresasInvestidas();

        assertFalse(dtos.size() == 0);
    }
    @Test
    public void deveListaTotalFaturado(){
        TotalRendimentosEmpresaDTO dto = this.comprarAcoesService.listaTotalFaturadoEmpresa(1l);

        assertFalse(dto.getPago().compareTo(new BigDecimal(100))> 0);

    }


    private ComprarAcoes getComprarAcoes(){

        ComprarAcoes comprarAcoes = new ComprarAcoes();
            comprarAcoes.setId(1l);
            comprarAcoes.setUsuario(getUsuario());
            comprarAcoes.setEmpresa(getEmpresa());
            comprarAcoes.setValorAcao(new BigDecimal(100));
            comprarAcoes.setValorpago(new BigDecimal(100));
            comprarAcoes.setTroco(new BigDecimal(100));
        return comprarAcoes;
    }
    private ComprarAcoesDTO getComprarAcoesDTO() {
        ComprarAcoesDTO comprarAcoesDTO = new ComprarAcoesDTO();
        comprarAcoesDTO.setId(1l);
        comprarAcoesDTO.setUsuario(getUsuario());
        comprarAcoesDTO.setEmpresa(getEmpresa());
        comprarAcoesDTO.setValorAcao(new BigDecimal(100));
        comprarAcoesDTO.setValorpago(new BigDecimal(100));
        comprarAcoesDTO.setTroco(new BigDecimal(100));
        return comprarAcoesDTO;
    }
    private Usuario getUsuario(){
        Usuario usuario = new Usuario();
        usuario.setId(1l);
        usuario.setNome("caio");
        usuario.setCpf("1");
        usuario.setCarteira(new BigDecimal(100));
        return usuario;
    }
    private Empresa getEmpresa(){
        Empresa empresa = new Empresa();
        empresa.setId(1l);
        empresa.setAcao("b1");
        empresa.setTicket("apple");
        empresa.setPreco(new BigDecimal(100));
        empresa.setStatus(ATIVO);
        empresa.setPercentualAcoes(100);
        return empresa;
    }
}