package com.banco.inter.model;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class UsuarioTest {

    @Test
    public void deveCriarUmUsuarioUsandoConstrutorComParametros() {
        Usuario usuario = new Usuario(1l,"maria","111",new BigDecimal(100));
        assertTrue(usuario.getId()== 1l);
        assertTrue(usuario.getNome().equals("maria"));
        assertTrue(usuario.getCpf().equals("111"));
        assertTrue(usuario.getCarteira().compareTo(new BigDecimal("100")) == 0);

    }
}