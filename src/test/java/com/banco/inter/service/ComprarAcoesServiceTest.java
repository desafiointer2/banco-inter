package com.banco.inter.service;

import com.banco.inter.dto.ComprarAcoesDTO;
import com.banco.inter.model.ComprarAcoes;
import com.banco.inter.model.Empresa;
import com.banco.inter.model.Usuario;
import com.banco.inter.repository.ComprarAcoesRepository;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static com.banco.inter.model.enuns.StatusEmpresa.ATIVO;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ComprarAcoesServiceTest {

    private ComprarAcoesRepository comprarAcoesRepository;

    public void deveComprarAcoes(){

        Mockito.when(comprarAcoesRepository.converterComprarAcoesEntidadeParaComprarAcoesDTO(getComprarAcoesDTO())).thenReturn(getComprarAcoes());
        Mockito.when(comprarAcoesRepository.save(getComprarAcoes())).thenReturn(getComprarAcoes());
        Mockito.when(comprarAcoesRepository.converterComprarAcoesDTOParaComprarAcoesEntidade(getComprarAcoes())).thenReturn(getComprarAcoesDTO());

        assertTrue(getComprarAcoesDTO().getId().equals(1l));

    }
    private ComprarAcoes getComprarAcoes() {
        ComprarAcoes comprarAcoes = new ComprarAcoes();
        comprarAcoes.setId(1l);
        comprarAcoes.setUsuario(getUsuario());
        comprarAcoes.setEmpresa(getEmpresa());
        comprarAcoes.setValorAcao(new BigDecimal(100));
        comprarAcoes.setValorpago(new BigDecimal(100));
        comprarAcoes.setTroco(new BigDecimal(100));

        return comprarAcoes;
    }

    private ComprarAcoesDTO getComprarAcoesDTO() {
        ComprarAcoesDTO comprarAcoesDTO = new ComprarAcoesDTO();
        comprarAcoesDTO.setId(1l);
        comprarAcoesDTO.setUsuario(getUsuario());
        comprarAcoesDTO.setEmpresa(getEmpresa());
        comprarAcoesDTO.setValorAcao(new BigDecimal(100));
        comprarAcoesDTO.setValorpago(new BigDecimal(100));
        comprarAcoesDTO.setTroco(new BigDecimal(100));
        return comprarAcoesDTO;
    }

    private Empresa getEmpresa(){
        Empresa empresa = new Empresa();
        empresa.setId(1l);
        empresa.setAcao("b1");
        empresa.setTicket("apple");
        empresa.setPreco(new BigDecimal(100));
        empresa.setStatus(ATIVO);
        empresa.setPercentualAcoes(100);
        return empresa;
    }
    private Usuario getUsuario(){
        Usuario usuario = new Usuario();
        usuario.setId(1l);
        usuario.setNome("caio");
        usuario.setCpf("1");
        usuario.setCarteira(new BigDecimal(100));
        return usuario;
    }
}