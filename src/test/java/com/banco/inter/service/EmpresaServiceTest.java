package com.banco.inter.service;

import static com.banco.inter.model.enuns.StatusEmpresa.ATIVO;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.banco.inter.dto.EmpresaDTO;
import com.banco.inter.model.Empresa;
import com.banco.inter.repository.EmpresaRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmpresaServiceTest {


    @Autowired
    public EmpresaRepository empresaRepository;


    public EmpresaService empresaService ;

    @Before
    public void setUp(){
        empresaService = new EmpresaService(empresaRepository);

    }

    @Test
    public void deveCriarUmaEmpresa() {

        EmpresaDTO empresaDTO = this.empresaService.criandoEmpresa(getEmpresaDTO());

        assertTrue(empresaDTO.getId() == 1l);
        assertTrue(empresaDTO.getTicket().equals("apple"));


    }

    @Test
    public void deveAtualzarUmaEmpresa(){


        EmpresaDTO empresaDTO = this.empresaService.criandoEmpresa(getEmpresaDTO());

        assertTrue(empresaDTO.getId() == 1l);
        assertTrue(empresaDTO.getTicket().equals("apple"));
    }

   @Test
   public void deveTestarORetornodaColecaoEMpresa(){
       List<EmpresaDTO> empresaDTOS = this.empresaService.listaDeEmpresas("ATIVO");

       assertTrue(empresaDTOS.size() == 0);

   }


    public Empresa getEmpresa(){
        Empresa empresa = new Empresa();
        empresa.setId(1l);
        empresa.setAcao("b1");
        empresa.setTicket("apple");
        empresa.setPreco(new BigDecimal(100));
        empresa.setStatus(ATIVO);
        empresa.setPercentualAcoes(100);
        return empresa;
    }
    public EmpresaDTO getEmpresaDTO() {
        EmpresaDTO empresaDTO = new EmpresaDTO();
        empresaDTO.setId(1l);
        empresaDTO.setAcao("b1");
        empresaDTO.setTicket("apple");
        empresaDTO.setPreco(new BigDecimal(100));
        empresaDTO.setStatus(ATIVO);
        empresaDTO.setPercentualAcoes(100);
        return empresaDTO;
    }
}