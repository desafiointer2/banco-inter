package com.banco.inter.service;

import com.banco.inter.dto.UsuarioDTO;
import com.banco.inter.model.Usuario;
import com.banco.inter.repository.UsuarioRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioServiceTest {



    @Autowired
    private UsuarioRepository usuarioRepositorio;

    private UsuarioService usuarioService;

    @Before
    public void setUp(){
        usuarioService = new UsuarioService(usuarioRepositorio);

    }


    @Test
    public void deveCriarUmUsuario(){

        UsuarioDTO usuarioDTO = this.usuarioService.criandoUsuario(getUsuarioDTO());

        assertTrue(usuarioDTO.getId() == 1l);
        assertTrue(usuarioDTO.getNome().equals("caio"));

    }
    private Usuario getUsuario(){
        Usuario usuario = new Usuario();
        usuario.setId(1l);
        usuario.setNome("caio");
        usuario.setCpf("555");
        usuario.setCarteira(new BigDecimal(100));
        return usuario;
    }
    private UsuarioDTO getUsuarioDTO(){
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setId(1l);
        usuarioDTO.setNome("caio");
        usuarioDTO.setCpf("555");
        usuarioDTO.setCarteira(new BigDecimal(100));
        return usuarioDTO;
    }
}