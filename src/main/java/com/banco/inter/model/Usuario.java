package com.banco.inter.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="tab_usuario")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	private String cpf;
	private BigDecimal carteira;
	
	
	
	public Usuario(Long id, String nome, String cpf, BigDecimal carteira) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.carteira = carteira;
	}
	
	
	public Usuario() {
		super();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public BigDecimal getCarteira() {
		return carteira;
	}

	public void setCarteira(BigDecimal carteira) {
		this.carteira = carteira;
	}
	
	

}
