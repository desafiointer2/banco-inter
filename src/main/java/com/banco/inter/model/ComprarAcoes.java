package com.banco.inter.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tab_compra_acoes")
public class ComprarAcoes {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;
	@ManyToOne
    @JoinColumn(name = "empresa_id")
    private Empresa empresa;
	private BigDecimal valorAcao;
	private BigDecimal valorpago;
	private BigDecimal troco;


	public ComprarAcoes(Long id, Usuario usuario, Empresa empresa, BigDecimal valorAcao, BigDecimal valorpago,
			BigDecimal troco) {
		this.id = id;
		this.usuario = usuario;
		this.empresa = empresa;
		this.valorAcao = valorAcao;
		this.valorpago = valorpago;
		this.troco = troco;
	}
	public ComprarAcoes() {

	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public BigDecimal getValorAcao() {
		return valorAcao;
	}
	public void setValorAcao(BigDecimal valorAcao) {
		this.valorAcao = valorAcao;
	}
	public BigDecimal getValorpago() {
		return valorpago;
	}
	public void setValorpago(BigDecimal valorpago) {
		this.valorpago = valorpago;
	}
	public BigDecimal getTroco() {
		return troco;
	}
	public void setTroco(BigDecimal troco) {
		this.troco = troco;
	}

	
	
}
