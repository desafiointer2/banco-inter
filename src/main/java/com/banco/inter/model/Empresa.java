package com.banco.inter.model;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.banco.inter.model.enuns.StatusEmpresa;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Empresa", description = "Representa uma empresa")
@Entity
@Table(name="tab_empresa")
public class Empresa {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String acao;
	
	private String ticket;
	
	private BigDecimal preco;
	
	@Enumerated(EnumType.STRING)
	private StatusEmpresa status;
	
	@Column(name="percentual_acoes")
	private int percentualAcoes;

	

	public Empresa(Long id, String acao, String ticket, BigDecimal preco, StatusEmpresa status, int percentualAcoes) {
		super();
		this.id = id;
		this.acao = acao;
		this.ticket = ticket;
		this.preco = preco;
		this.status = status;
		this.percentualAcoes = percentualAcoes;
	}
	public Empresa() {}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getAcao() {
		return acao;
	}


	public void setAcao(String acao) {
		this.acao = acao;
	}


	public String getTicket() {
		return ticket;
	}


	public void setTicket(String ticket) {
		this.ticket = ticket;
	}


	public BigDecimal getPreco() {
		return preco;
	}


	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}


	public StatusEmpresa getStatus() {
		return status;
	}


	public void setStatus(StatusEmpresa status) {
		this.status = status;
	}


	public int getPercentualAcoes() {
		return percentualAcoes;
	}


	public void setPercentualAcoes(int percentualAcoes) {
		this.percentualAcoes = percentualAcoes;
	}
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		return Objects.equals(id, other.id);
	}

	
}
