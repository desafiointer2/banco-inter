package com.banco.inter.model.enuns;

public enum StatusEmpresa {

	ATIVO,
	INATIVO
}
