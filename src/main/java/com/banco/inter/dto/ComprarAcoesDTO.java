package com.banco.inter.dto;

import java.math.BigDecimal;

import com.banco.inter.model.Empresa;
import com.banco.inter.model.Usuario;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ComprarAcoes", description = "Representa uma compra de ação")
public class ComprarAcoesDTO {
	
	@ApiModelProperty(example = "1", required = false)
	private Long id;

	@ApiModelProperty(example = "Caio", required = true)
    private Usuario usuario;
	
	@ApiModelProperty(example = "Banco Inter", required = true)
	private Empresa empresa;
	
	@ApiModelProperty(example = "150.99", required = true)
	private BigDecimal valorAcao;
	
	@ApiModelProperty(example = "150.99", required = true)
	private BigDecimal valorpago;
	
	@ApiModelProperty(example = "150.99", required = true)
	private BigDecimal troco;
	
	
	public ComprarAcoesDTO() {
	}
	

	public ComprarAcoesDTO(Long id, Usuario usuario, Empresa empresa,
			BigDecimal valorAcao, BigDecimal valorpago, BigDecimal troco) {
		this.id = id;
		this.usuario = usuario;
		this.empresa = empresa;
		this.valorAcao = valorAcao;
		this.valorpago = valorpago;
		this.troco = troco;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public Empresa getEmpresa() {
		return empresa;
	}


	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}


	public BigDecimal getValorAcao() {
		return valorAcao;
	}


	public void setValorAcao(BigDecimal valorAcao) {
		this.valorAcao = valorAcao;
	}


	public BigDecimal getValorpago() {
		return valorpago;
	}


	public void setValorpago(BigDecimal valorpago) {
		this.valorpago = valorpago;
	}


	public BigDecimal getTroco() {
		return troco;
	}


	public void setTroco(BigDecimal troco) {
		this.troco = troco;
	}
	
	
	
}
