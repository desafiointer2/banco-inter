package com.banco.inter.dto;

public class InputComprarAcoesDTO {
	
	private Long idEmpresa;
	private Long idUsuario;
	
	
	
	public InputComprarAcoesDTO(Long idEmpresa, Long idUsuario) {
		this.idEmpresa = idEmpresa;
		this.idUsuario = idUsuario;
	}
	
	public InputComprarAcoesDTO() {
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	

}
