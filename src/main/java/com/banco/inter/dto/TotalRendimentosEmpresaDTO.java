package com.banco.inter.dto;

import java.math.BigDecimal;

public class TotalRendimentosEmpresaDTO {

	private BigDecimal troco;
	private BigDecimal pago;
	
	
	public TotalRendimentosEmpresaDTO(BigDecimal troco, BigDecimal pago) {
		this.troco = troco;
		this.pago = pago;
	}

	public BigDecimal getTroco() {
		return troco;
	}
	public void setTroco(BigDecimal troco) {
		this.troco = troco;
	}
	public BigDecimal getPago() {
		return pago;
	}
	public void setPago(BigDecimal pago) {
		this.pago = pago;
	}

	
	
}
