package com.banco.inter.dto;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Usuario", description = "Representa um usuario")
public class UsuarioDTO {
	
	@ApiModelProperty(example = "1", required = false)
	private Long id;
	
	@ApiModelProperty(example = "Caio", required = true)
	private String nome;
	
	@ApiModelProperty(example = "123.558.555-56", required = true)
	private String cpf;
	
	@ApiModelProperty(example = "259.99", required = true)
	private BigDecimal carteira;
	
	public UsuarioDTO() {
	}
	public UsuarioDTO(Long id, String nome, String cpf, BigDecimal carteira) {
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.carteira = carteira;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public BigDecimal getCarteira() {
		return carteira;
	}
	public void setCarteira(BigDecimal carteira) {
		this.carteira = carteira;
	}
	
	
}
