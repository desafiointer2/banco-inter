package com.banco.inter.dto;

import java.math.BigDecimal;

import com.banco.inter.model.enuns.StatusEmpresa;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Empresa", description = "Representa uma empresa")
public class EmpresaDTO {
		
	@ApiModelProperty(example = "1", required = false)
	private Long id;
	
	@ApiModelProperty(example = "apple", required = true)
	private String acao;
	
	@ApiModelProperty(example = "bi0011", required = true)
	private String ticket;
	
	@ApiModelProperty(example = "259.99", required = true)
	private BigDecimal preco;
	
	@ApiModelProperty(example = "ATIVO", required = true)
	private StatusEmpresa status;
	
	@ApiModelProperty(example = "100", required = true)
	private int percentualAcoes;

	
	public EmpresaDTO(Long id, String acao, String ticket, BigDecimal preco, StatusEmpresa status,
			int percentualAcoes) {
		this.id = id;
		this.acao = acao;
		this.ticket = ticket;
		this.preco = preco;
		this.status = status;
		this.percentualAcoes = percentualAcoes;
	}

	public EmpresaDTO() {
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public StatusEmpresa getStatus() {
		return status;
	}

	public void setStatus(StatusEmpresa status) {
		this.status = status;
	}

	public int getPercentualAcoes() {
		return percentualAcoes;
	}

	public void setPercentualAcoes(int percentualAcoes) {
		this.percentualAcoes = percentualAcoes;
	}


	
}
