package com.banco.inter.config.springfox;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringFoxConfig implements WebMvcConfigurer {

	@Bean
	public Docket apiDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
					.apis(RequestHandlerSelectors.any())
					.build()
				.apiInfo(apiInfo()).tags(new Tag("Empresa", "Gerencia de empresas"),
						new Tag("Usuario", "Gerencia usuarios"),
						new Tag("Comprar Ações", "Gerencia para compras de ações"));
	}

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
    
    public ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("API Investimento Inter")
				.description("API Para processo seletivo do banco Inter")
				.version("1")
				.contact(new Contact("Caio Santos", "AQUI VAI O LINK DO GITLAB", "caiohgs13@gmail.com"))
				.build();
	}
}