package com.banco.inter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banco.inter.converter.ConverterUsuario;
import com.banco.inter.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, ConverterUsuario {
	

	Optional<Usuario> findByCpf(String cpf);
	
}
