package com.banco.inter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.banco.inter.converter.ConverterCompraAcoes;
import com.banco.inter.dto.TotalRendimentosEmpresaDTO;
import com.banco.inter.model.ComprarAcoes;

@Repository
public interface ComprarAcoesRepository extends JpaRepository<ComprarAcoes, Long>, ConverterCompraAcoes {

	@Query("select c from ComprarAcoes c where c.usuario.id = :id")
	List<ComprarAcoes> filtroPorInvestimentos(@Param("id") Long id );
	
	@Query("select new com.banco.inter.dto.TotalRendimentosEmpresaDTO(sum(c.troco),sum(c.valorpago)) from ComprarAcoes c where c.empresa.id = :id")
	TotalRendimentosEmpresaDTO totalFaturado(@Param("id") Long id );
	
}

