package com.banco.inter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.banco.inter.converter.ConverterEmpresa;
import com.banco.inter.model.Empresa;
import com.banco.inter.model.enuns.StatusEmpresa;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long>, ConverterEmpresa {

	@Query("select e from Empresa e where e.status = :status")
	List<Empresa> filtroEmpresasPorStatus(@Param("status") StatusEmpresa status);
	
	

}
	