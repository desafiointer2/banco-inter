package com.banco.inter.exception;

public class UsuarioNaoEncontradoException extends EntidadeNaoEncontradaException {

	
	private static final long serialVersionUID = 1L;
	
	public UsuarioNaoEncontradoException(String mensagem) {
		super(mensagem);
	}


}
