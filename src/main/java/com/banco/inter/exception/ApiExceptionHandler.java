package com.banco.inter.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(EntidadeNaoEncontradaException.class)
	public ResponseEntity<?> tratarEntidadeNaoEncontradaException(
			EntidadeNaoEncontradaException e) {
			
		
		Problem problema = new Problem();
		
		problema.setDataHora(LocalDateTime.now());
		problema.setMensagem(e.getMessage());
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(problema);
	}
	
	@ExceptionHandler(UsuarioNaoEncontradoException.class)
	public ResponseEntity<?> tratarNegocioException(UsuarioNaoEncontradoException e) {
		Problem problema = new Problem();
		
		problema.setDataHora(LocalDateTime.now());
		problema.setMensagem(e.getMessage());
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(problema);
	}
}