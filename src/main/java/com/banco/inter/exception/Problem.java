package com.banco.inter.exception;

import java.time.LocalDateTime;

public class Problem {

	private LocalDateTime dataHora;
	private String mensagem;
	
	
	
	public Problem(LocalDateTime dataHora, String mensagem) {
		super();
		this.dataHora = dataHora;
		this.mensagem = mensagem;
	}
	
	
	public Problem() {
		super();
	}

	public LocalDateTime getDataHora() {
		
		return dataHora;
	}

	public void setDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
	
}