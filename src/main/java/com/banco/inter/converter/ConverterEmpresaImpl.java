package com.banco.inter.converter;

import org.springframework.stereotype.Service;

import com.banco.inter.dto.EmpresaDTO;
import com.banco.inter.model.Empresa;

@Service
public class ConverterEmpresaImpl implements ConverterEmpresa {

	@Override
	public EmpresaDTO converterEmpresaEntidadeParaEmpresaDTO(Empresa empresa) {
		// TODO Auto-generated method stub
		return new EmpresaDTO(
				empresa.getId(),
				empresa.getAcao(),
				empresa.getTicket(),
				empresa.getPreco(),
				empresa.getStatus(),
				empresa.getPercentualAcoes()
				);
	}

	@Override
	public Empresa converterEmpresaDTOParaEmpresaEntidade(EmpresaDTO empresaDTO) {
		// TODO Auto-generated method stub
		return new Empresa(
				empresaDTO.getId(),
				empresaDTO.getAcao(),
				empresaDTO.getTicket(),
				empresaDTO.getPreco(),
				empresaDTO.getStatus(),
				empresaDTO.getPercentualAcoes()
				);
	}

}
