package com.banco.inter.converter;

import com.banco.inter.dto.ComprarAcoesDTO;
import com.banco.inter.model.ComprarAcoes;
import com.banco.inter.model.Empresa;
import com.banco.inter.model.Usuario;

public class ConverterCompraAcoesImpl implements ConverterCompraAcoes {

	@Override
	public ComprarAcoes converterComprarAcoesEntidadeParaComprarAcoesDTO(ComprarAcoesDTO comprarAcoesDTO) {
		
		return new ComprarAcoes(
				comprarAcoesDTO.getId(),
				comprarAcoesDTO.getUsuario(),
				comprarAcoesDTO.getEmpresa(),
				comprarAcoesDTO.getValorAcao(),
				comprarAcoesDTO.getValorpago(),
				comprarAcoesDTO.getTroco()
				);
				
	}

	@Override
	public ComprarAcoesDTO converterComprarAcoesDTOParaComprarAcoesEntidade(ComprarAcoes comprarAcoes) {

		return new ComprarAcoesDTO(
				comprarAcoes.getId(),
				comprarAcoes.getUsuario(),
				comprarAcoes.getEmpresa(),
				comprarAcoes.getValorAcao(),
				comprarAcoes.getValorpago(),
				comprarAcoes.getTroco()
				);
	}

	@Override
	public ComprarAcoesDTO criandoObjetoParaPersistenciaNoBanco(Empresa empresa, Usuario usuario) {
		ComprarAcoesDTO dto = new ComprarAcoesDTO();
		dto.setEmpresa(empresa);
		dto.setUsuario(usuario);
		dto.setValorAcao(empresa.getPreco());
		dto.setValorpago(usuario.getCarteira());
		dto.setTroco(usuario.getCarteira().subtract(empresa.getPreco()));
		return dto;
	}

}
