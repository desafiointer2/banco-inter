package com.banco.inter.converter;

import com.banco.inter.dto.ComprarAcoesDTO;
import com.banco.inter.model.ComprarAcoes;
import com.banco.inter.model.Empresa;
import com.banco.inter.model.Usuario;

public interface ConverterCompraAcoes {

	ComprarAcoes converterComprarAcoesEntidadeParaComprarAcoesDTO(ComprarAcoesDTO comprarAcoesDTO);		
	
	ComprarAcoesDTO converterComprarAcoesDTOParaComprarAcoesEntidade(ComprarAcoes comprarAcoes);
	
	ComprarAcoesDTO criandoObjetoParaPersistenciaNoBanco(Empresa empresa, Usuario usuario);
}
