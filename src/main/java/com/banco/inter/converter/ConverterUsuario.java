package com.banco.inter.converter;

import com.banco.inter.dto.UsuarioDTO;
import com.banco.inter.model.Usuario;

public interface ConverterUsuario  {

	UsuarioDTO converterUsuarioEntidadeParaUsuarioDTO(Usuario usuario);		
	
	Usuario converterUsuarioDTOParaUsuarioEntidade(UsuarioDTO usuarioDTO);
}
