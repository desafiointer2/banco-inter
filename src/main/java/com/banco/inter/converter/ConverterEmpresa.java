package com.banco.inter.converter;

import com.banco.inter.dto.EmpresaDTO;
import com.banco.inter.model.Empresa;

public interface ConverterEmpresa {

	EmpresaDTO converterEmpresaEntidadeParaEmpresaDTO(Empresa empresa);		
	
	Empresa converterEmpresaDTOParaEmpresaEntidade(EmpresaDTO empresaDTO);
}
