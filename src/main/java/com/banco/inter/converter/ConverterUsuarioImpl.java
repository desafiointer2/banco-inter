package com.banco.inter.converter;

import org.springframework.stereotype.Service;

import com.banco.inter.dto.UsuarioDTO;
import com.banco.inter.model.Usuario;

@Service
public class ConverterUsuarioImpl implements ConverterUsuario {

	
	@Override
	public UsuarioDTO converterUsuarioEntidadeParaUsuarioDTO(Usuario usuario) {

		return new UsuarioDTO (
				usuario.getId(),
				usuario.getNome(),
				usuario.getCpf(),
				usuario.getCarteira());
	}

	@Override
	public Usuario converterUsuarioDTOParaUsuarioEntidade(UsuarioDTO usuarioDTO) {

		return new Usuario (
				usuarioDTO.getId(),
				usuarioDTO.getNome(),
				usuarioDTO.getCpf(),
				usuarioDTO.getCarteira());
	}

}
