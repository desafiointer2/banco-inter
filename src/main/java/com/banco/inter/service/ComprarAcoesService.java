package com.banco.inter.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banco.inter.dto.ComprarAcoesDTO;
import com.banco.inter.dto.EmpresaDTO;
import com.banco.inter.dto.InputComprarAcoesDTO;
import com.banco.inter.dto.TotalRendimentosEmpresaDTO;
import com.banco.inter.exception.EmpresaNaoEncontradaException;
import com.banco.inter.exception.UsuarioNaoEncontradoException;
import com.banco.inter.model.ComprarAcoes;
import com.banco.inter.model.Empresa;
import com.banco.inter.model.Usuario;
import com.banco.inter.model.enuns.StatusEmpresa;
import com.banco.inter.repository.ComprarAcoesRepository;
import com.banco.inter.repository.EmpresaRepository;
import com.banco.inter.repository.UsuarioRepository;

@Service
public class ComprarAcoesService {
	
	private UsuarioRepository usuarioRepositorio;
	private EmpresaRepository empresaRepository;
	private ComprarAcoesRepository comprarAcoesRepository;
	private static final String MSG_EMPRESA_NAO_ENCONTRADA = "Empresa de código %d não encontrado.";
	private static final String MSG_USUARIO_NAO_ENCONTRADO = "Usuario não Encontrado.";
	private static final String MSG_USUARIO_NAO_TEM_SALDO = "Saldo Insuficiente";

	@Autowired
	public ComprarAcoesService(UsuarioRepository usuarioRepositorio, EmpresaRepository empresaRepository,
			ComprarAcoesRepository comprarAcoesRepository) {
		this.usuarioRepositorio = usuarioRepositorio;
		this.empresaRepository = empresaRepository;
		this.comprarAcoesRepository = comprarAcoesRepository;
	}



	public ComprarAcoesDTO comprarAcoes(InputComprarAcoesDTO inputComprarAcoesDTO) {
		
		Empresa empresa = validarEmpresa(inputComprarAcoesDTO.getIdEmpresa());
		Usuario usuario = validarUsuario(inputComprarAcoesDTO.getIdUsuario());
		
		if(validarCompra(empresa, usuario)) {
			throw new UsuarioNaoEncontradoException(MSG_USUARIO_NAO_TEM_SALDO);

		}
		ComprarAcoesDTO objetoMontadoAcao = this.comprarAcoesRepository.criandoObjetoParaPersistenciaNoBanco(empresa, usuario);
		
		ComprarAcoes comprarAcoes = this.comprarAcoesRepository.converterComprarAcoesEntidadeParaComprarAcoesDTO(objetoMontadoAcao);
		ComprarAcoes comprarAcoesCriado = this.comprarAcoesRepository.save(comprarAcoes);
		ComprarAcoesDTO dto = this.comprarAcoesRepository.converterComprarAcoesDTOParaComprarAcoesEntidade(comprarAcoesCriado);
		this.atualizaValorCarteira(dto, inputComprarAcoesDTO.getIdUsuario());
		return dto;
		
	}

	private Usuario validarUsuario(Long id) {
		
		Usuario usuario = this.visualizarUsuario(id);

		if(usuario.getCarteira().signum() == 0) {
			throw new UsuarioNaoEncontradoException(MSG_USUARIO_NAO_TEM_SALDO);
		}
		

		return usuario;
	}



	private Empresa validarEmpresa(Long id) {
		
		Empresa empresa = this.visualizarEmpresa(id);

		if (empresa.getStatus() == StatusEmpresa.ATIVO) {

			return empresa;
		}

		return null;
		
		
	}
	private Empresa visualizarEmpresa(Long id) {
		return this.empresaRepository.findById(id)
                .orElseThrow(() -> new EmpresaNaoEncontradaException(
                        String.format(MSG_EMPRESA_NAO_ENCONTRADA, id)
                ));
	}
	
	private boolean validarCompra(Empresa empresa, Usuario usuario) {
		
		if (usuario.getCarteira().compareTo(empresa.getPreco()) == -1) {
			return true;
		}
		return false;
	}
	
	private Usuario visualizarUsuario(Long id) {
		return this.usuarioRepositorio.findById(id)
                .orElseThrow(() -> new EmpresaNaoEncontradaException(
                        String.format(MSG_USUARIO_NAO_ENCONTRADO, id)
                ));
	}



	public List<ComprarAcoesDTO> listaDeAcoesCompradas(Long id) {

		List<ComprarAcoes> acoes = this.comprarAcoesRepository.filtroPorInvestimentos(id);
		List<ComprarAcoesDTO> dtos = acoes.stream()
				.map(e -> this.comprarAcoesRepository.converterComprarAcoesDTOParaComprarAcoesEntidade(e))
				.collect(Collectors.toList());
		return dtos;
		
	}
	public void atualizaValorCarteira(ComprarAcoesDTO acoesDTO, Long id ) {
		Usuario usuario = this.visualizarUsuario(id);
		usuario.setCarteira(acoesDTO.getTroco());
		this.usuarioRepositorio.save(usuario);
		}



	public List<ComprarAcoesDTO> listaPorEmpresasInvestidas() {
		
		List<ComprarAcoes> acoes = this.comprarAcoesRepository.findAll();
		List<ComprarAcoesDTO> dtos = acoes.stream()
				.map(e -> this.comprarAcoesRepository.converterComprarAcoesDTOParaComprarAcoesEntidade(e))
				.collect(Collectors.toList());
		return dtos;

		
	}

	public TotalRendimentosEmpresaDTO listaTotalFaturadoEmpresa(Long id) {
		TotalRendimentosEmpresaDTO dto = this.comprarAcoesRepository.totalFaturado(id);
		return dto;
	}
	

}
