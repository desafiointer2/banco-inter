package com.banco.inter.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banco.inter.dto.EmpresaDTO;
import com.banco.inter.exception.EmpresaNaoEncontradaException;
import com.banco.inter.model.Empresa;
import com.banco.inter.model.enuns.StatusEmpresa;
import com.banco.inter.repository.EmpresaRepository;

@Service
public class EmpresaService {
	
	
	private EmpresaRepository empresaRepository;
	private static final String MSG_EMPRESA_NAO_ENCONTRADA
    = "Empresa de código %d não encontrado.";
	
	@Autowired
	public EmpresaService(EmpresaRepository empresaRepository) {
		this.empresaRepository = empresaRepository;
	}

	public EmpresaDTO criandoEmpresa(EmpresaDTO empresaDTO) {

		Empresa empresa = this.empresaRepository.converterEmpresaDTOParaEmpresaEntidade(empresaDTO);
		Empresa empresaCriada = this.empresaRepository.save(empresa);
		EmpresaDTO dto = this.empresaRepository.converterEmpresaEntidadeParaEmpresaDTO(empresaCriada);

		return dto;
	}

	public EmpresaDTO atualizandoEmpresa(Long id, EmpresaDTO empresaDTO) {
		Empresa empresa = visualizarEmpresa(id);
		BeanUtils.copyProperties(empresaDTO, empresa, "id", "acao", "ticket", "percentualAcoes","status");
		Empresa empresaEditada = this.empresaRepository.save(empresa);
		EmpresaDTO dto = this.empresaRepository.converterEmpresaEntidadeParaEmpresaDTO(empresaEditada);
		return dto;
	}
	
	

	public void deletarEmpresa(Long id) {
		Empresa empresa = visualizarEmpresa(id);
		 this.empresaRepository.delete(empresa);
	}

	public List<EmpresaDTO> listaDeEmpresas(String status) {
		
		if(status != null ) {
			List<Empresa> empresas = this.empresaRepository.filtroEmpresasPorStatus(getStatusEmpresa(status));
			List<EmpresaDTO> dtos = empresas.stream()
					.map(e -> this.empresaRepository.converterEmpresaEntidadeParaEmpresaDTO(e))
					.collect(Collectors.toList());
			return dtos;
		}
		List<Empresa> empresas = this.empresaRepository.findAll();
		List<EmpresaDTO> dtos = empresas.stream()
				.map(e -> this.empresaRepository.converterEmpresaEntidadeParaEmpresaDTO(e))
				.collect(Collectors.toList());
		return dtos;
	}
	
	private StatusEmpresa getStatusEmpresa(String status) {
		return StatusEmpresa.valueOf(status);
	
	}
	private Empresa visualizarEmpresa(Long id) {
		return this.empresaRepository.findById(id)
                .orElseThrow(() -> new EmpresaNaoEncontradaException(
                        String.format(MSG_EMPRESA_NAO_ENCONTRADA, id)
                ));
	}


}
