package com.banco.inter.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banco.inter.dto.UsuarioDTO;
import com.banco.inter.exception.UsuarioNaoEncontradoException;
import com.banco.inter.model.Usuario;
import com.banco.inter.repository.UsuarioRepository;

@Service
public class UsuarioService {


	private UsuarioRepository usuarioRepositorio;
	private static final String MSG_USUARIO_NAO_ENCONTRADO
    = "Usuario ja cadastrado.";


	@Autowired
	public UsuarioService(UsuarioRepository usuarioRepositorio) {
		this.usuarioRepositorio = usuarioRepositorio;
	}

	public UsuarioDTO criandoUsuario(UsuarioDTO usuarioDTO) {
		String cpf = usuarioDTO.getCpf();
		Optional<Usuario> usuarioInvalido = this.usuarioRepositorio.findByCpf(cpf);

		if(usuarioInvalido.isPresent()) {
			throw new UsuarioNaoEncontradoException(MSG_USUARIO_NAO_ENCONTRADO);
		}
		Usuario usuario = this.usuarioRepositorio.converterUsuarioDTOParaUsuarioEntidade(usuarioDTO);
		Usuario usuarioCriado = this.usuarioRepositorio.save(usuario);
		UsuarioDTO dto = this.usuarioRepositorio.converterUsuarioEntidadeParaUsuarioDTO(usuarioCriado);

		return dto;
		
	}
	
}
