package com.banco.inter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banco.inter.controller.openapi.ComprarAcoesControllerOpenApi;
import com.banco.inter.dto.ComprarAcoesDTO;
import com.banco.inter.dto.InputComprarAcoesDTO;
import com.banco.inter.dto.TotalRendimentosEmpresaDTO;
import com.banco.inter.service.ComprarAcoesService;

@RestController
@RequestMapping("/comprarAcoes")
public class ComprarAcoesController implements ComprarAcoesControllerOpenApi {

	ComprarAcoesService comprarAcoesService;
	
	@Autowired
	public ComprarAcoesController(ComprarAcoesService comprarAcoesService) {
		this.comprarAcoesService = comprarAcoesService;
	}
	
	@PostMapping
	public ResponseEntity<ComprarAcoesDTO> comprarAcoes(@RequestBody InputComprarAcoesDTO inputComprarAcoesDTO){
		ComprarAcoesDTO dto = this.comprarAcoesService.comprarAcoes(inputComprarAcoesDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}
	
	@GetMapping("/lista-por-acoes-compradas/{id}")
	public ResponseEntity<List<ComprarAcoesDTO>> listaDeAcoesCompradas(@PathVariable Long id){
		
		List<ComprarAcoesDTO> dtos = this.comprarAcoesService.listaDeAcoesCompradas(id);
		return ResponseEntity.ok().body(dtos);
	}
	
	@GetMapping("/lista-por-investimento")
	public ResponseEntity<List<ComprarAcoesDTO>> listaPorEmpresasInvestidas(){
		
		List<ComprarAcoesDTO> dtos = this.comprarAcoesService.listaPorEmpresasInvestidas();
		return ResponseEntity.ok().body(dtos);
	}
	
	@GetMapping("/total-faturamento/{id}")
	public ResponseEntity<TotalRendimentosEmpresaDTO> listaTotalFaturadoEmpresa(@PathVariable Long id){
		
		 TotalRendimentosEmpresaDTO dtos = this.comprarAcoesService.listaTotalFaturadoEmpresa(id);
		
		return ResponseEntity.ok().body(dtos);
	}
	
}
