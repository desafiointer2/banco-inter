package com.banco.inter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banco.inter.controller.openapi.UsuarioControllerOpenApi;
import com.banco.inter.dto.UsuarioDTO;
import com.banco.inter.service.UsuarioService;

import io.swagger.annotations.Api;

@Api(tags = "Usuario")
@RestController
@RequestMapping("/usuarios")
public class UsuarioController implements UsuarioControllerOpenApi {

	UsuarioService usuarioService;
	
	@Autowired
	public UsuarioController(UsuarioService service) {
		this.usuarioService = service;
	}
	
	@PostMapping
	public ResponseEntity<UsuarioDTO> criandoUsuario(@RequestBody UsuarioDTO usuarioDTO){
		UsuarioDTO dto = this.usuarioService.criandoUsuario(usuarioDTO);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}
	
}
