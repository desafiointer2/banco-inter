package com.banco.inter.controller.openapi;

import org.springframework.http.ResponseEntity;

import com.banco.inter.dto.UsuarioDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Usuario")
public interface UsuarioControllerOpenApi {

	@ApiOperation("Cria usuario na aplicação")
	@ApiResponses({ @ApiResponse(code = 201, message = "Retorno do metodo é uma Usuário"), })
	public ResponseEntity<UsuarioDTO> criandoUsuario(
			@ApiParam(name = "Usuario", value = "Representação de uma novo usuário") UsuarioDTO usuarioDTO);

}
