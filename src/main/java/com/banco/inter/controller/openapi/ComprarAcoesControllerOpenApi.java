package com.banco.inter.controller.openapi;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.banco.inter.dto.ComprarAcoesDTO;
import com.banco.inter.dto.InputComprarAcoesDTO;
import com.banco.inter.dto.TotalRendimentosEmpresaDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;

@Api(tags = "Comprar Ações")
public interface ComprarAcoesControllerOpenApi {
	
	
	@ApiOperation("Compra de Ações das Empresas")
	@ApiResponse(code = 201, message = "Retorno do metodo é uma Compra de Ações")
	public ResponseEntity<ComprarAcoesDTO> comprarAcoes(@ApiParam(value = "Compra de Ações", example = "Representacao de Compra de Ações")InputComprarAcoesDTO inputComprarAcoesDTO);
	
	@ApiOperation("Lista de Ações compradas das empresas na aplicação")
	public ResponseEntity<List<ComprarAcoesDTO>> listaDeAcoesCompradas(@ApiParam(value = "parametro para realizar filtro", example = "1")Long id);
	
	@ApiOperation("Lista de Ações das empresas investidas na aplicação")
	public ResponseEntity<List<ComprarAcoesDTO>> listaPorEmpresasInvestidas();
	
	@ApiOperation("Mostra o total faturado de venda das ações")
	public ResponseEntity<TotalRendimentosEmpresaDTO> listaTotalFaturadoEmpresa(@ApiParam(value = "parametro para realizar filtro", example = "1") Long id);

}
