package com.banco.inter.controller.openapi;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.banco.inter.dto.EmpresaDTO;
import com.banco.inter.exception.Problem;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Empresa")
public interface EmpresaControllerOpenApi {

	@ApiOperation("Lista todas as empresas de acordo com o filtro")
	public ResponseEntity<List<EmpresaDTO>> listaDeEmpresas(@ApiParam(value = "parametros para realizar filtro", example = "ATIVO|INATIVO") String status);

	
	@ApiOperation("Cria empresa na aplicação")
	@ApiResponses({ @ApiResponse(code = 201, message = "Retorno do metodo é uma Empresa"), })
	public ResponseEntity<EmpresaDTO> criandoEmpresa(@ApiParam(name = "Empresa", value = "Representação de uma nova empresa") EmpresaDTO empresaDTO);

	@ApiOperation("Atualiza uma empresa por ID")
	@ApiResponses({ @ApiResponse(code = 200, message = "Empresa atualizada"),
			@ApiResponse(code = 404, message = "Empresa não encontrada", response = Problem.class) })
	public ResponseEntity<EmpresaDTO> atualizandoEmpresa(@ApiParam(value = "ID de uma empresa", example = "1") Long cidadeId,
			@ApiParam(name = "corpo", value = "Representação de uma empresa com os novos dados") EmpresaDTO empresaDTO);

	@ApiOperation("Deleta a empresa")
	@ApiResponses({ @ApiResponse(code = 204, message = "Empresa excluída"),
			@ApiResponse(code = 404, message = "Empresa não encontrada", response = Problem.class) })
	public ResponseEntity<Void> deletarEmpresa(@ApiParam(value = "ID de uma empresa", example = "1") Long id);

}
