package com.banco.inter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banco.inter.controller.openapi.EmpresaControllerOpenApi;
import com.banco.inter.dto.EmpresaDTO;
import com.banco.inter.service.EmpresaService;

@RestController
@RequestMapping("/empresas")
public class EmpresaController implements EmpresaControllerOpenApi {

	private EmpresaService empresaService;

	@Autowired
	public EmpresaController(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@GetMapping
	public ResponseEntity<List<EmpresaDTO>> listaDeEmpresas(@RequestParam(required = false) String status) {
		List<EmpresaDTO> empresas = this.empresaService.listaDeEmpresas(status);
		return ResponseEntity.ok().body(empresas);
	}
	
	@PostMapping
	public ResponseEntity<EmpresaDTO> criandoEmpresa(@RequestBody EmpresaDTO empresaDTO){
		EmpresaDTO dto = this.empresaService.criandoEmpresa(empresaDTO);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<EmpresaDTO> atualizandoEmpresa(@PathVariable Long id, @RequestBody EmpresaDTO empresaDTO){
		EmpresaDTO dto = this.empresaService.atualizandoEmpresa(id, empresaDTO);
		
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletarEmpresa(@PathVariable Long id){
		
		 this.empresaService.deletarEmpresa(id);

		return ResponseEntity.ok().build();

	}
		
}
